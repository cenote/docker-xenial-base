# xenial-base

Based on ubuntu:16.04

This Docker image is the base image for the java build images.
It contains any software that the other images have in common.

Installed software:

* apt-transport-https
* ca-certificates
* curl
* fontconfig (making ttf-mscorefonts available to java)
* git
* libstdc++6:i386 (needed for nsis)
* nsis-2.46
* openssh-client
* ttf-mscorefonts-installer
* wget
